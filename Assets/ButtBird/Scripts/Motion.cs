using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class Motion : MonoBehaviour
{
    [System.Serializable]
    public class Hand {
        public Vector3 Acceleration;
        public Vector3 Orientation;
        [ReadOnly]
        public Vector3 WorldAcceleration;
        public UnityEvent<Vector3> OnChangeHandler;
    }

    [Header("Constants")]
    [SerializeField] private float gravity = 1.0f;
    [SerializeField] private float liftForce = 25.0f;
    [SerializeField] private float maxPitch = 45f;
    [SerializeField] private float maxRoll = 35f;
    [SerializeField] private float turnRate = 0.1f;

    [Header("Left Hand")]
    [SerializeField] private Hand left = new Hand();

    [Header("Right Hand")]
    [SerializeField] private Hand right = new Hand();

    [Header("PID")]
    [SerializeField] private PIDController pid = new PIDController();

    private Rigidbody rb;
    private Vector3 force;
    private Vector3 originalOrientation;

    [Space]

    [Header("Debug Information")]
    [ReadOnly]
    [SerializeField]
    private float currentPitch = 0f;
    [ReadOnly]
    [SerializeField]
    private float currentRoll = 0f;

    [Space]

    [ReadOnly]
    [SerializeField]
    private float targetPitch = 0f;
    [ReadOnly]
    [SerializeField]
    private float targetRoll = 0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.maxAngularVelocity = 2f;
        originalOrientation = transform.eulerAngles;
        ControllerLookup.Instance.RegisterBird(gameObject);
    }

    void OnDestroy()
    {
        ControllerLookup.Instance.UnregisterBirds();
    }

    // Update is called once per frame
    void Update()
    {
        // if (rb.velocity.y > 0.01f) {
        //     return;
        // }

        // var forwardSpeed = Vector3.Project(rb.velocity, transform.forward).magnitude;

        // rb.AddForce(Vector3.up * (forwardSpeed * liftForce), ForceMode.Force);
    }

    void FixedUpdate()
    {
        if (left.Orientation.x == 0f || right.Orientation.x == 0f) {
            return;
        }

        targetPitch = Mathf.Clamp((left.Orientation.x + right.Orientation.x) / 2f, -maxPitch, maxPitch);
        targetRoll = Mathf.Clamp(left.Orientation.x - right.Orientation.x, -maxRoll, maxRoll);

        var deltaPitch = pid.UpdateAngle(Time.fixedDeltaTime, currentPitch, targetPitch);
        var deltaRoll = pid.UpdateAngle(Time.fixedDeltaTime, currentRoll, targetRoll);

        currentPitch += deltaPitch;
        currentRoll += deltaRoll;

        currentPitch = Mathf.Clamp(currentPitch, -maxPitch, maxPitch);
        currentRoll = Mathf.Clamp(currentRoll, -maxRoll, maxRoll);

        var deltaYaw = currentRoll * currentPitch * turnRate;
        //rb.AddRelativeTorque(deltaPitch, 0f, deltaRoll, ForceMode.Force);
        //transform.eulerAngles = new Vector3(currentPitch, transform.eulerAngles.y + deltaYaw, currentRoll);
        //transform.eulerAngles = new Vector3(currentPitch, 0f, currentRoll);
        //transform.Rotate(deltaPitch, 0f, deltaRoll);
        transform.eulerAngles = new Vector3(originalOrientation.x, originalOrientation.y, originalOrientation.z + currentRoll);
    }
    
    public void Accelerate(float value, Vector3 point)
    {
        rb.AddForce(transform.rotation * (Quaternion.AngleAxis(liftForce, Vector3.left) * Vector3.forward) * value, ForceMode.Impulse);
        //rb.AddForceAtPosition(transform.rotation * (Quaternion.AngleAxis(liftForce, Vector3.left) * Vector3.forward) * value, transform.localToWorldMatrix * point, ForceMode.Impulse);
    }

    private void OnHandChanged(Hand hand)
    {
        var worldOrientation = Quaternion.Euler(hand.Orientation);
        var worldAcceleration = worldOrientation * hand.Acceleration;
        worldAcceleration.y += gravity;
        hand.WorldAcceleration = worldAcceleration;
        hand.OnChangeHandler.Invoke(hand.WorldAcceleration);
    }

    public void OnAccelerationChangedLeft(Vector3 acceleration)
    {
        left.Acceleration = acceleration;
        OnHandChanged(left);
    }

    public void OnAccelerationChangedRight(Vector3 acceleration)
    {
        right.Acceleration = acceleration;
        OnHandChanged(right);
    }

    public void OnOrientationChangedLeft(Vector3 orientation)
    {
        left.Orientation = orientation;
        OnHandChanged(left);
    }

    public void OnOrientationChangedRight(Vector3 orientation)
    {
        right.Orientation = orientation;
        OnHandChanged(right);
    }
}
