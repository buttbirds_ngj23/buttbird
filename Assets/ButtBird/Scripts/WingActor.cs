using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingActor : MonoBehaviour
{
    private Transform body;
    private Motion parentMotion;
    private Vector3 wingPosition;
    [ReadOnly]
    [SerializeField]
    private Vector3 wingAngle = new Vector3();

    [SerializeField] private float wingSwingDirection = 1.0f;
    [SerializeField] private float maxAngle = 35f;
    [SerializeField] private float accelerationFlapFactor = 0.2f;
    [SerializeField] private float accelerationDeadZone = 0.1f;
    [SerializeField] private float accelerationForceFactor = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        body = transform.parent;
        wingPosition = transform.localPosition;
        parentMotion = GetComponentInParent<Motion>();
        if (parentMotion == null) {
            Debug.LogError("No Motion script found in parent");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnAccelerationChanged(Vector3 acceleration)
    {
        if (Mathf.Abs(acceleration.y) < accelerationDeadZone) {
            return;
        }

        wingAngle.z = Mathf.Clamp(wingAngle.z - (acceleration.y * accelerationFlapFactor * wingSwingDirection), -maxAngle, maxAngle);
        transform.localEulerAngles = wingAngle;

        //var position = transform.localPosition;
        //position.y += acceleration.y * accelerationFlapFactor;
        //position.y = Mathf.Clamp(position.y, -maxHeight, maxHeight);
        //position.Normalize();
        //position *= wingSpan;
        //transform.localPosition = position;

        if (acceleration.y < 0f || parentMotion == null) {
            return;
        }
        parentMotion.Accelerate(acceleration.y * accelerationForceFactor, wingPosition);
    }
}
