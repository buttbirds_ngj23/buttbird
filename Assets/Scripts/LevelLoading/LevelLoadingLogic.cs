using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.Events;

public class LevelLoadingLogic : Singleton<LevelLoadingLogic>
{
    [SerializeField] private GameObject[] ToDisable;
    [SerializeField] private float VideoLengthTimeSec = 5.0f;
    [SerializeField] private float IntroTimeLengthSecs = 5.0f;
    [SerializeField] private string InitialScene;
    [SerializeField] private List<string> sceneNames;
    public UnityEvent OnGameStart;

    private void Start()
    {
        StartCoroutine(LoadScenesSequence());
        
    }

    IEnumerator LoadScenesSequence()
    {
        yield return new WaitForSeconds(VideoLengthTimeSec);
        SceneManager.LoadSceneAsync(InitialScene, LoadSceneMode.Additive);
        yield return new WaitForSeconds(IntroTimeLengthSecs);
        while (!ControllerLookup.Instance.IsConnected) {
            yield return new WaitForSeconds(0.1f);
        }
        for (int i = 0; i < ToDisable.Length; i++)
        {
            ToDisable[i].gameObject.SetActive(false);
        }
        LoadScenes();
        OnGameStart.Invoke();
    }

    private void LoadScenes()
    {

        // for (int i = 0; i < SceneManager.sceneCount; i++) {
        //     SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
        // }

        if (sceneNames.Count > 0)
        {
            for (int i = 0; i < sceneNames.Count; i++)
            {
                SceneManager.LoadSceneAsync(sceneNames[i], LoadSceneMode.Additive);
            }    
        }
    }
}