using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T> {

    public static T Instance { get; private set; }

    private void Awake() {
        // Check if an instance already exists
        if (Instance != null && Instance != this) {
            Destroy(gameObject); // Destroy this gameobject to ensure only one instance exists
        } else {
            Instance = (T)this; // Set the singleton instance
            DontDestroyOnLoad(gameObject); // Persist the gameobject across scenes
        }
    }

    // Your other code here
}