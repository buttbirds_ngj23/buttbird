using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ClappingScript : MonoBehaviour
{
    [SerializeField] private Vector2 MinMaxSoundVolume;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField] private AudioClip[] ClappingSound;
    
    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log("CLAP!");
        SoundSingletonHelper.Instance.PlaySound(ClappingSound, _audioSource, MinMaxSoundVolume);
    }
}
