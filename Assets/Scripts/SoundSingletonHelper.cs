using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSingletonHelper : Singleton<SoundSingletonHelper>
{
    public void PlaySound(AudioClip[] clips, AudioSource audioSource, Vector2 VolMinMax)
    {
        AudioClip clipToPlay = clips[Random.Range(0, clips.Length - 1)];
        audioSource.clip = clipToPlay;
        audioSource.volume = Random.Range(VolMinMax.x, VolMinMax.y);
        audioSource.pitch = Random.Range(0.95f, 1.05f);
        
        audioSource.Play();
    }
}
