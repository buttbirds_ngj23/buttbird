using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TurnOffCanvasAfterSecs : MonoBehaviour
{
    private void Start()
    {
        //StartCoroutine(TurnOffAfter5Secs());
        LevelLoadingLogic.Instance.OnGameStart.AddListener(delegate { GetComponent<Canvas>().enabled = false; });
    }

    IEnumerator TurnOffAfter5Secs()
    {
        yield return new WaitForSeconds(5.0f);
        Canvas canvas = GetComponent<Canvas>();
        canvas.enabled = false;
    }
}
