using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class ButtholeShitting : MonoBehaviour
{
    [SerializeField] private GameObject TurdGO;
    [SerializeField] private int Turds = 5;
    private bool CanUnloadTurd = true;
    private float SecondsBetweenFeces = 3.0f;
    private IEnumerable<TurdCounterOnTarget> Targets;

    public float ScoringPoints = 0.0f;

    private void OnGUI()
    {
        string Label = "Points: " + ScoringPoints;
        GUI.Label(new Rect(10, 10, 100, 20), Label);
    }

    private void Start()
    {
        Targets = Resources.FindObjectsOfTypeAll<TurdCounterOnTarget>();
    }

    private void Update()
    {
        ScoringPoints = 0.0f;
        foreach (var Target in Targets)
        {
            float minMagnitude = float.MaxValue;
            TurdLogic closestTurd = null;
            var turds = Target.turds;
            for (int i = 0; i < Target.turds.Count; i++)
            {
                if (turds[i])
                {
                    float distanceFromTarget = Vector3.Magnitude(Target.transform.position - turds[i].transform.position);
                    if (distanceFromTarget < minMagnitude)
                    {
                        minMagnitude = distanceFromTarget;
                        closestTurd = turds[i];
                    }
                }
            }

            if (minMagnitude < float.MaxValue)
            {
                ScoringPoints += Mathf.Clamp(100f - minMagnitude,0.0f, 100.0f);
            }
        }
    }
    
    

    public void UnloadTurd()
    {
        if (CanUnloadTurd && Turds > 0)
        {
            CanUnloadTurd = false;
            GameObject Turd = Instantiate(TurdGO, this.gameObject.transform.position, this.gameObject.transform.rotation);
            Turds--;
            StartCoroutine(UnloadTurdCounter());
        }
    }

    IEnumerator UnloadTurdCounter()
    {
        yield return new WaitForSeconds(SecondsBetweenFeces);
        CanUnloadTurd = true;
    }
}
