using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LEGOWirelessSDK;

public class HubRegistration : MonoBehaviour
{
    public bool IsLeft;

    public void OnConnectedChanged(bool connected)
    {
        if (!connected) {
            return;
        }

        if (IsLeft) {
            ControllerLookup.Instance.RegisterLeft(gameObject);
        } else {
            ControllerLookup.Instance.RegisterRight(gameObject);
        }
    }

    public void ConnectHub()
    {
        GetComponent<HubBase>().FindDeviceToUse();
    }

    void Start()
    {
    }
}
