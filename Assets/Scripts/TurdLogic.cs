using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class TurdLogic : MonoBehaviour
{
    [SerializeField] private Vector2 SoundMinMaxVol;
    [SerializeField] private AudioClip[] ShitFalling;
    [SerializeField] private AudioClip[] ShitHitting;
    [SerializeField] private GameObject ShitPrefab;
    [SerializeField] private AudioSource ShitAudioSource;

    [SerializeField] private string[] NamesOfObjectToIgnore = new [] {"Armature", "Bird_V1"};

    private void Start()
    {
        SoundSingletonHelper.Instance.PlaySound(ShitFalling, ShitAudioSource, SoundMinMaxVol);
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        bool ignoreTurdCollision = false;
        for (int i = 0; i < NamesOfObjectToIgnore.Length; i++)
        {
            if (collision.gameObject.name.Contains(NamesOfObjectToIgnore[i]))
            {
                ignoreTurdCollision = true;
                break;
            }
        }

        if (ignoreTurdCollision)
        {
            return;
        }
        
        ShitAudioSource.Stop();
        SoundSingletonHelper.Instance.PlaySound(ShitHitting, ShitAudioSource, SoundMinMaxVol);

        Rigidbody rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.detectCollisions = false;
        Quaternion poopRotation = Quaternion.Euler(0.0f, Random.Range(0f, 360f), 0.0f);
        GameObject shit = Instantiate(ShitPrefab, this.gameObject.transform.position,
            poopRotation);
            
        Debug.Log("Hitting: " + collision.gameObject.name);
    }
}
