using System;
using UnityEngine;

public class PlaneMovement : MonoBehaviour
{
    public float speed = 1.0f;  // Speed of movement
    public float distance = 5.0f;  // Distance to move

    private Vector3 startPos;  // Starting position of the plane
    private float timer = 0.0f;  // Timer to keep track of movement direction

    void Start()
    {
        startPos = transform.position;  // Save starting position
    }

    void Update()
    {
        // Move the plane back and forth along the x-axis
        timer += Time.deltaTime * speed;
        float newX = Mathf.Sin(timer) * distance;
        transform.position = startPos + new Vector3(newX, 0.0f, 0.0f);
    }
}