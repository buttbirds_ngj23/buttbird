using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class KappaLauncherScript : MonoBehaviour
{
    public Vector2 MinMaxFlysound;
    public Vector2 MinMaxAwakingSound;
    public AudioClip[] KappaFlyingSound;
    public AudioClip[] KappAwaking;
    public AudioSource FlyingKappaSourceFly;
    public AudioSource AwakiningSource;
    
    
    public float DistanceToActivate = 200.0f;
    public float MaxSpeed = 50.0f;
    private ButtholeShitting buttHoleBird;
    public float lerptime = 5f;
    public float _kappaTimeBeforeHitLerp = 3.0f;
    private float TimeSinceStartFlying = 0.0f;
    private bool Activated = false;
    private float speed = 0.0f;
    private Vector3 DirectionToGoAfterLerp = Vector3.zero;
    public float SpeedAfterLerp = 20.0f;
    private Vector3 LerpStartPos = Vector3.zero;

    private void Start()
    {
        buttHoleBird = GameObject.FindObjectOfType<ButtholeShitting>();
        if (buttHoleBird == null)
        {
            Debug.LogError("KAPPA CANT FIND BUTTHOLE!! MAMMA MIAAA!");
        }
    }

    private void Update()
    {
        if (Activated == false &&
            Vector3.Magnitude(buttHoleBird.transform.position - this.transform.position) < DistanceToActivate)
        {
            SoundSingletonHelper.Instance.PlaySound(KappAwaking, AwakiningSource, MinMaxAwakingSound);
            SoundSingletonHelper.Instance.PlaySound(KappaFlyingSound, FlyingKappaSourceFly, MinMaxFlysound);
            Activated = true;
            LerpStartPos = this.transform.position;
        }
        
        if (Activated)
        {
            TimeSinceStartFlying += Time.deltaTime;
            if (TimeSinceStartFlying <= _kappaTimeBeforeHitLerp)
            {
                Vector3 lerpPos = Vector3.Lerp(LerpStartPos, buttHoleBird.transform.position,
                    TimeSinceStartFlying / lerptime);
                transform.position = lerpPos;
                Debug.Log("Letp time: " + (TimeSinceStartFlying / lerptime) + " Kappatimebefore: " + _kappaTimeBeforeHitLerp);
            }
            else
            {
                Debug.Log("Fuori if, prima di if");

                if (DirectionToGoAfterLerp == Vector3.zero)
                {
                    Debug.Log("Maremma mia sono dentro l'if");       
                    DirectionToGoAfterLerp =
                        Vector3.Normalize(buttHoleBird.transform.position - this.transform.position);
                }

                Vector3 newPosition = transform.position + (DirectionToGoAfterLerp * (Time.deltaTime * SpeedAfterLerp));
                transform.position = newPosition;
            }
        }
    }
}
