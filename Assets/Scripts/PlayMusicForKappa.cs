using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayMusicForKappa : MonoBehaviour
{
    [SerializeField] private AudioClip kappaMusicClip;

    private AudioSource source;
    private AudioClip originalClip;
    private bool startedPlaying = false;

    private void Awake()
    {
        GameObject audio = GameObject.Find("Audio");
        source = audio.GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains("Bird") || other.name.Contains("bird") && startedPlaying == false)
        {
            startedPlaying = true;
            originalClip = source.clip;
            source.Stop();
            source.clip = kappaMusicClip;
            source.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name.Contains("Bird") || other.name.Contains("bird") && startedPlaying == true)
        {
            startedPlaying = false;
            source.Stop();
            source.clip = originalClip;
            source.Play();
        }
    }
}
