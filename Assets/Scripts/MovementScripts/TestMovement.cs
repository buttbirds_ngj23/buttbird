using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.Events;
using Vector3 = UnityEngine.Vector3;

public class TestMovement : MonoBehaviour
{
    [SerializeField] private float Flyforce = 100.0f;
    private Rigidbody BirdBody;
    [SerializeField]private UnityEvent ShitNow;
    
    private void Start()
    {
        BirdBody = GetComponent<Rigidbody>();
        if (BirdBody == null)
        {
            Debug.LogError("No rigid body");
        }
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow))
        {
            BirdBody.AddForce(Vector3.up * Flyforce);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            ShitNow.Invoke();
        }
    }
}
