using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LEGOWirelessSDK;

public class ControllerLookup : Singleton<ControllerLookup>
{
    private GameObject leftController;
    private GameObject rightController;

    public bool IsConnected {
        get {
            return leftController != null && rightController != null;
        }
    }

    public void RegisterLeft(GameObject obj)
    {
        if (obj.GetComponent<HubBase>() == null) {
            return;
        }
        leftController = obj;
    }

    public void RegisterRight(GameObject obj)
    {
        if (obj.GetComponent<HubBase>() == null) {
            return;
        }
        rightController = obj;
    }

    public void RegisterBird(GameObject bird)
    {
        for (var motion = bird.GetComponent<Motion>(); motion != null; motion = null) {
            leftController.GetComponent<AccelerationSensor>().AccelerationChanged.AddListener(motion.OnAccelerationChangedLeft);
            leftController.GetComponent<OrientationSensor>().OrientationChanged.AddListener(motion.OnOrientationChangedLeft);
            rightController.GetComponent<AccelerationSensor>().AccelerationChanged.AddListener(motion.OnAccelerationChangedRight);
            rightController.GetComponent<OrientationSensor>().OrientationChanged.AddListener(motion.OnOrientationChangedRight);
        }
        var butthole = bird.GetComponentInChildren<ButtholeShitting>(false);
        if (butthole != null) {
            leftController.GetComponent<HubBase>().ButtonChanged.AddListener(delegate (bool pressed) { butthole.UnloadTurd(); });
            rightController.GetComponent<HubBase>().ButtonChanged.AddListener(delegate (bool pressed) { butthole.UnloadTurd(); });
        }
    }

    public void UnregisterBirds()
    {
        leftController.GetComponent<AccelerationSensor>().AccelerationChanged.RemoveAllListeners();
        leftController.GetComponent<OrientationSensor>().OrientationChanged.RemoveAllListeners();
        rightController.GetComponent<AccelerationSensor>().AccelerationChanged.RemoveAllListeners();
        rightController.GetComponent<OrientationSensor>().OrientationChanged.RemoveAllListeners();
        leftController.GetComponent<HubBase>().ButtonChanged.RemoveAllListeners();
        rightController.GetComponent<HubBase>().ButtonChanged.RemoveAllListeners();
    }
}
